import webapp as wp
import pagesfile as pf
from urllib import parse
import string
import random


class shortener(wp.webapp):

    def __init__(self, host, port):
        self.urls_dict = {}
        super().__init__(host, port)

    def parse(self, received):
        body_start = received.find('\r\n\r\n')

        if body_start == -1:
            body = None
        else:
            body = received[body_start + 4:]

        msg_shards = received.split(' ')
        method = msg_shards[0]
        resource = msg_shards[1]

        return method, resource, body

    def process(self, analyzed):
        method, resource, body = analyzed

        if method == "GET":
            http_code, html_code = self.get(resource)
        elif method == "POST":
            http_code, html_code = self.post(resource, body)
        else:
            http_code, html_code = "405 Method not allowed", \
                pf.PAGE_NOT_ALLOWED.format(method=method)

        return (http_code, html_code)

    def compose_url_list(self):
        url_list = ""
        for key in self.urls_dict.keys():
            url_list += key + " --> " + self.urls_dict.get(key) + "<br/>"

        return url_list

    def proc_url(self, url):
        processed_url = url
        if not "http://" in url or "https://" in url:
            processed_url = "https://" + processed_url

        shorted = ''.join(random.choices(string.ascii_lowercase, k=6))

        return processed_url, shorted


    def get(self, resource):
        if resource == "/":
            code = "200 OK"
            url_list = self.compose_url_list()
            print("URL list: " + url_list)
            page = pf.STANDARD_PAGE.format(url_list=url_list, form=pf.FORM)
        else:
            resource = resource[1:]
            if resource in self.urls_dict.keys():
                code = "301 Moved Permanently \r\n" \
            	    + "Location: " + self.urls_dict[resource]
                page = ""
            else:
                code = "404 Resource Not Found"
                page = pf.PAGE_NOT_FOUND

        return code, page

    def post(self, resource, body):
        post_body = parse.parse_qs(body)
        print("Fields in post body: ", post_body)
        if resource == "/":
            url_to_short, shorted = self.proc_url(post_body["url"][0])

            if not url_to_short in self.urls_dict.values():
                self.urls_dict[shorted] = url_to_short

            url_list = self.compose_url_list()
            print("URL list: " + url_list)
            code = "200 OK"
            page = pf.STANDARD_PAGE.format(url_list=url_list, form=pf.FORM)

        else:
            code = "405 Method not allowed"
            page = pf.PAGE_NOT_ALLOWED.format(method='POST')

        return code, page


if __name__ == "__main__":
    testWeb = shortener('localhost', 1230)
